package test

import (
	"bigint/bigint"
	"fmt"
	"testing"
)

func TestBigint(t *testing.T) {
	// new int

	a := "16516213541621"
	if _, err := bigint.NewInt(a); !err {
		t.Errorf("not number")
	}

	a = "56465sdf%6498"
	if _, err := bigint.NewInt(a); err {
		t.Errorf("not number")
	}

	a = "-6483-21"
	if _, err := bigint.NewInt(a); !err {
		t.Errorf("not number")
	}

	fmt.Println("NEW INT PASSES")

	//SET

	// b:=bigint.Set()

}
