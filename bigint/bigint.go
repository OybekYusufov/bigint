package bigint

import (
	"strconv"
	"strings"
)

type bigint struct {
	num string
}

func NewInt(num string) (bigint, bool) {
	var e bigint
	if number(num) {
		e = bigint{num}
		return e, true
	} else {
		return e, false
	}
}
func number(num string) bool {
	x := []rune(num)
	for i := 0; i < len(num); i++ {
		if (string(x[i]) >= "a" && string(x[i]) <= "z") || (string(x[i]) >= "A" && string(x[i]) <= "Z") {
			return false
		}
	}
	return true
}
func (z *bigint) Set(num string) error {
	z.num = num
	return nil
}
func (x *bigint) Abs() bigint {
	var c bigint
	if strings.Contains(x.num, "-") {
		c.num = strings.TrimLeft(x.num, "-")
	} else {
		c.num = x.num
	}
	return c
}
func Add(a bigint, b bigint) bigint {
	if strings.Contains(b.num,"-") && strings.Contains(a.num,"-"){
		b.num = strings.TrimLeft(b.num, "-")
		a.num = strings.TrimLeft(a.num, "-")
		var sum bigint = Add(a, b)
		sum.num="-"+sum.num
		return sum
	}else if strings.Contains(b.num, "-") {
		b.num = strings.TrimLeft(b.num, "-")
		var sum bigint = Sub(a, b)
		return sum
	} else if strings.Contains(a.num, "-") {
		a.num = strings.TrimLeft(a.num, "-")
		var sum bigint = Sub(b, a)
		return sum
	} else {
		r := Reverce(a.num)
		t := Reverce(b.num)
		xotira := 0
		s := ""
		var k int
		if len(a.num) > len(b.num) {
			k = len(a.num)
			for i := 0; i < k-len(b.num); i++ {
				t += "0"
			}
		} else if len(a.num) < len(b.num) {
			k = len(b.num)
			for i := 0; i < k-len(a.num); i++ {
				r += "0"
			}
		} else {
			k = len((a.num))
		}
		var a1 int
		var a2 int
		var err error
		x := []rune(r)
		y := []rune(t)
		for i := 0; i < k; i++ {

			if a1, err = strconv.Atoi(string(x[i])); err == nil {
				if a2, err = strconv.Atoi(string(y[i])); err == nil {
					n := a1 + a2 + xotira
					if n >= 10 {
						xotira = 1
						m := n % 10
						s += strconv.Itoa(m)
					} else {
						s += strconv.Itoa(n)
						xotira = 0
					}
				}
			}
		}

		m, e := NewInt(Reverce(s))
		if e {

		}
		return m
	}
}

func Sub(a bigint, b bigint) bigint {
	if strings.Contains(b.num, "-") {
		b.num = strings.TrimLeft(b.num, "-")
		var sum bigint = Add(a, b)
		return sum
	} else if strings.Contains(a.num, "-") {
		a.num = strings.TrimLeft(a.num, "-")
		var sum bigint = Add(b, a)
		sum.num = "-" + sum.num
		return sum
	} else {
		r := Reverce(a.num)
		t := Reverce(b.num)
		xotira := 0
		s := ""
		var k int
		var q bool = true
		if len(a.num) > len(b.num) {
			k = len(a.num)
			for i := 0; i < k-len(b.num); i++ {
				t += "0"
			}
		} else if len(a.num) < len(b.num) {
			k = len(b.num)
			for i := 0; i < k-len(a.num); i++ {
				r += "0"
			}
			q = false
		} else {
			k = len((a.num))
		}
		var a1 int
		var a2 int
		var err error
		var x []rune
		var y []rune
		if q {
			x = []rune(r)
			y = []rune(t)
		} else {
			x = []rune(t)
			y = []rune(r)
		}
		for i := 0; i < k; i++ {

			if a1, err = strconv.Atoi(string(x[i])); err == nil {
				if a2, err = strconv.Atoi(string(y[i])); err == nil {
					n := a1 - a2 + xotira
					if n < 0 {
						xotira = -1
						m := n + 10
						s += strconv.Itoa(m)
					} else {
						s += strconv.Itoa(n)
						xotira = 0
					}
				}
			}
		}
		if !q {
			s += "-"
		}

		m, e := NewInt(Reverce(s))
		if e {

		}
		return m
	}
}

func Multiply(a bigint, b bigint) bigint {
	var tk bool=false
	if strings.Contains(b.num,"-") || strings.Contains(a.num,"-"){
		tk=true
	}
	r := Reverce(a.num)
	t := Reverce(b.num) + "0"
	xotira := 0
	s := ""
	var a1 int
	var a2 int
	var err error
	w, f := NewInt("0")
	if f {
	}
	x := []rune(r)
	y := []rune(t)
	for i := 0; i < len(a.num); i++ {

		if a1, err = strconv.Atoi(string(x[i])); err == nil {
			xotira = 0
			s = ""
			for j := 0; j < len(b.num); j++ {
				if a2, err = strconv.Atoi(string(y[j])); err == nil {
					n := a1*a2 + xotira
					if n >= 10 {
						if j == len(b.num)-1 {
							s += Reverce(strconv.Itoa(n))
							xotira = 0
						} else {
							xotira = n / 10
							m := n % 10
							s += strconv.Itoa(m)
						}
					} else if n < 10 {
						s += strconv.Itoa(n)
						xotira = 0
					}

				}

			}
			for l := 0; l < i; l++ {
				s = "0" + s
			}
			s = Reverce(s)
			m, e := NewInt(s)
			if e {

			}
			w = Add(w, m)
		}
	}
	if tk{
		w.num="-"+w.num
	}
	return w
}

func Reverce(x string) string {
	y := ""
	chars := []rune(x)
	var k int = len(x)
	for i := k - 1; i >= 0; i-- {
		y += string(chars[i])
	}
	return y
}
